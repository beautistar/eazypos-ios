//
//  PrinterCell.swift
//  EazyPOS
//
//  Created by Beautistar on 28/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class PrinterCell: UITableViewCell {

    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnKickDrawer: UIButton!
    @IBOutlet weak var btnTestPrint: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btnClose.layer.borderColor = UIColor.white.cgColor
        btnTestPrint.layer.borderColor = UIColor.white.cgColor
        btnKickDrawer.layer.borderColor = UIColor.white.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
