//
//  StockLevelCell.swift
//  EazyPOS
//
//  Created by Beautistar on 7/29/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class StockLevelCell: UICollectionViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.gray.cgColor
    }
}
