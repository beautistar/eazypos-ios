//
//  CustomerListCell.swift
//  EazyPOS
//
//  Created by Beautistar on 07/07/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class CustomerListCell: UITableViewCell {

    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnView: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
