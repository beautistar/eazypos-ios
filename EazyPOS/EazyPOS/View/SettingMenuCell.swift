//
//  SettingMenuCell.swift
//  EazyPOS
//
//  Created by Beautistar on 26/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class SettingMenuCell: UITableViewCell {

    @IBOutlet weak var imvIcon: UIImageView!
    @IBOutlet weak var lblMenu: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
