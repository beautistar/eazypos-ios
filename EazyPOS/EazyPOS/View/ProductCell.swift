//
//  ProductCell.swift
//  EazyPOS
//
//  Created by Beautistar on 6/6/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.masksToBounds = true;
        self.layer.cornerRadius = 5.0
    }
}
