//
//  R.swift
//  EazyPOS
//
//  Created by Beautistar on 7/29/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

struct R {
    
    struct string {
        
        static let APP_TITLE = "Krepta"
        
        static let OK = "Ok"
        static let CANCEL = "Cancel"
        static let YES = "YES"
        static let NO = "NO"
        static let ERROR = "Error Occured!"
        static let VOID_SALE = "All items added in the product cart will be removed.\nAre you sure you wish to VOID this sale?"
        
        
        static let CONNECT_FAIL = "Connection to the server failed.\nPlease try again."
        static let UPLOAD_FAIL = "Failed to file send."
        
        static let AGREE_TERM = "To signup Riparadiese, \nYou have to agree to Terms & Privacy Policy"
        static let INPUT_USERNAME = "Please input vaild username."
        
        static let EMAIL_EXIST = "Email already exists."
              //        static let CHORE_STATE = ["Working", "Completed", "Checked", "Paid", "Declined"]
    }
}
