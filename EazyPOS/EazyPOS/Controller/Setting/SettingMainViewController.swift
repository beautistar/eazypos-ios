//
//  SettingMainViewController.swift
//  EazyPOS
//
//  Created by Beautistar on 26/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class SettingMainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var containerView: UIView!
    var currentViewController:UIViewController!
    
    var icons = ["ic_status", "ic_reports", "ic_printer", "ic_scanner", "ic_payments", "ic_configuration", "ic_advanced"]
    var menus = ["STATUS", "REPORTS", "PRINTERS", "BARCODE SCANNER", "PAYMENT", "CONFIGURATION", "ADVANCED"]
    
    private lazy var statusViewController: StatusViewController = {
        
        // Instantiate View Controller
        var viewController = self.storyboard?.instantiateViewController(withIdentifier: "StatusViewController") as! StatusViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var reportViewController: ReportViewController = {
        
        // Instantiate View Controller
        var viewController = self.storyboard?.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var barcodeViewController: BarcodeViewController = {
        
        // Instantiate View Controller
        var viewController = self.storyboard?.instantiateViewController(withIdentifier: "BarcodeViewController") as! BarcodeViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var printerViewController: PrinterSettingViewController = {
        
        // Instantiate View Controller
        var viewController = self.storyboard?.instantiateViewController(withIdentifier: "PrinterSettingViewController") as! PrinterSettingViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var paymentViewController: PaymentSettingViewController = {
        
        // Instantiate View Controller
        var viewController = self.storyboard?.instantiateViewController(withIdentifier: "PaymentSettingViewController") as! PaymentSettingViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var configViewController: ConfigSettingViewController = {
        
        // Instantiate View Controller
        var viewController = self.storyboard?.instantiateViewController(withIdentifier: "ConfigSettingViewController") as! ConfigSettingViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var advancedViewController: AdvancedViewController = {
        
        // Instantiate View Controller
        var viewController = self.storyboard?.instantiateViewController(withIdentifier: "AdvancedViewController") as! AdvancedViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblMenu.tableFooterView = UIView()
        btnBack.layer.borderColor = UIColor.white.cgColor
        currentViewController = statusViewController
        //updateView(currentAddedViewController, statusViewController)
        //add(statusViewController)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.dismiss(animated:false, completion:nil)
    }
    
    
    //MARK: - TableView Datasource Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return icons.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingMenuCell") as! SettingMenuCell
        cell.imvIcon.image = UIImage.init(named: icons[indexPath.row])
        cell.lblMenu.text = menus[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        lblTitle.text = menus[indexPath.row]
        
        switch indexPath.row {
        
        case 0:
            remove(asChildViewController: currentViewController)
            add(asChildViewController: statusViewController)
            currentViewController = statusViewController
            break
        
        case 2:
            remove(asChildViewController: currentViewController)
            add(asChildViewController: printerViewController)
            currentViewController = printerViewController
            break
        
        case 3:
            remove(asChildViewController: currentViewController)
            add(asChildViewController: barcodeViewController)
            currentViewController = barcodeViewController
            break
            
        case 4:
            remove(asChildViewController: currentViewController)
            add(asChildViewController: paymentViewController)
            currentViewController = paymentViewController
            break
            
        case 5:
            remove(asChildViewController: currentViewController)
            add(asChildViewController: configViewController)
            currentViewController = configViewController
            break
            
        case 6:
            remove(asChildViewController: currentViewController)
            add(asChildViewController: advancedViewController)
            currentViewController = advancedViewController
            break
            
        
        default:
            remove(asChildViewController: currentViewController)
            add(asChildViewController: reportViewController)
            currentViewController = reportViewController
            break
        }
    }
    
    // MARK: - Helper Methods
    
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChildViewController(viewController)
        
        // Add Child View as Subview
        containerView.addSubview(viewController.view)
        
        // Configure Child View
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParentViewController: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParentViewController()
    }

}
