//
//  PrinterSettingViewController.swift
//  EazyPOS
//
//  Created by Beautistar on 28/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class PrinterSettingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var btnSearchPrint: UIButton!
    @IBOutlet weak var btnAddManualPrinter: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        btnSearchPrint.layer.borderColor = UIColor.white.cgColor
        btnAddManualPrinter.layer.borderColor = UIColor.white.cgColor
    }
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 150
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PrinterCell") as! PrinterCell
         return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
