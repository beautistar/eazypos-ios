//
//  PurchaseViewController.swift
//  EazyPOS
//
//  Created by Beautistar on 07/07/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Presentr

class PurchaseViewController: UIViewController,UISearchBarDelegate,  UITableViewDataSource, UITableViewDelegate {
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.transitionType = TransitionType.coverHorizontalFromRight
        presenter.dismissOnSwipe = true
        return presenter
    }()

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var backButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addCustomerAction(_ sender: Any) {
        
        
        let addCustomerVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCustomerViewController") as! AddCustomerViewController
        
        // Create the dialog
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage:0.6), height: ModalSize.fluid(percentage:0.95), center: ModalCenterPosition.center)
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        presenter.cornerRadius = 10
        presenter.roundCorners = true
        customPresentViewController(presenter, viewController: addCustomerVC, animated: true, completion: nil)
        
    }
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated:true)
    }
    
    // MARK: - SearchBar delegate
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        /*
         filteredData = searchText.isEmpty ? data : data.filter { (item: String) -> Bool in
         // If dataItem matches the searchText, return true to include it
         return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
         }
         
         tableView.reloadData()
         */
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
//        tblSearchedUserList.isHidden = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
//        tblSearchedUserList.isHidden = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    //MARK: - tableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerListCell") as! CustomerListCell
        cell.btnAdd.tag = indexPath.row
        cell.btnAdd.addTarget(self, action:#selector(gotoAdd), for: .touchUpInside)
        
        cell.btnView.tag = indexPath.row
        cell.btnView.addTarget(self, action:#selector(gotoView), for: .touchUpInside)
        return cell
    }
    
    func gotoAdd(_ sender:UIButton) {
        
        let addPurchaseVC = self.storyboard?.instantiateViewController(withIdentifier: "AddPurchaseViewController") as! AddPurchaseViewController
        
        self.navigationController?.pushViewController( addPurchaseVC, animated:true)
    }
    
    func gotoView(_ sender:UIButton) {
        
        let addPurchaseVC = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseDetailViewController") as! PurchaseDetailViewController
        
        self.navigationController?.pushViewController( addPurchaseVC, animated:true)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
