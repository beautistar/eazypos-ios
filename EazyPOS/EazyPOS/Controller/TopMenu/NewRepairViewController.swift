//
//  NewRepairViewController.swift
//  EazyPOS
//
//  Created by Beautistar on 29/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class NewRepairViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfContactNo: UITextField!
    @IBOutlet weak var tfModel: UITextField!
    @IBOutlet weak var tfImei: UITextField!
    @IBOutlet weak var tfFault: UITextField!
    @IBOutlet weak var tfNote: UITextField!
    @IBOutlet weak var tfQuote: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tfName.delegate = self
        tfContactNo.delegate = self
        tfModel.delegate = self
        tfImei.delegate = self
        tfFault.delegate = self
        tfNote.delegate = self
        tfQuote.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
    
        super.viewWillAppear(animated)
    
        initView()
    }
    
    func initView() {
        
        tfName.layer.borderWidth = 2.0
        tfContactNo.layer.borderWidth = 2.0
        tfModel.layer.borderWidth = 2.0
        tfImei.layer.borderWidth = 2.0
        tfFault.layer.borderWidth = 2.0
        tfNote.layer.borderWidth = 2.0
        tfQuote.layer.borderWidth = 2.0
        
        
        tfName.layer.borderColor = UIColor.white.cgColor
        tfContactNo.layer.borderColor = UIColor.white.cgColor
        tfModel.layer.borderColor = UIColor.white.cgColor
        tfImei.layer.borderColor = UIColor.white.cgColor
        tfFault.layer.borderColor = UIColor.white.cgColor
        tfNote.layer.borderColor = UIColor.white.cgColor
        tfQuote.layer.borderColor = UIColor.white.cgColor
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }

    //MARK: - UITextFieldDelegate 
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (tfName.isFirstResponder) {
            tfContactNo.becomeFirstResponder()
        } else if(tfContactNo.isFirstResponder) {
            tfModel.becomeFirstResponder()
        } else if(tfModel.isFirstResponder) {
            tfImei.becomeFirstResponder()
        } else if(tfImei.isFirstResponder) {
            tfFault.becomeFirstResponder()
        } else if(tfFault.isFirstResponder) {
            tfNote.becomeFirstResponder()
        } else if(tfNote.isFirstResponder) {
            tfQuote.becomeFirstResponder()
        } else if(tfQuote.isFirstResponder) {
            tfQuote.returnKeyType = UIReturnKeyType.done
            tfQuote.resignFirstResponder()
        }
        
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
