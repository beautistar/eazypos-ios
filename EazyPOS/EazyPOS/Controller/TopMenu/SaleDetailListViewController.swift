//
//  SaleDetailListViewController.swift
//  EazyPOS
//
//  Created by Beautistar on 29/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class SaleDetailListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var backButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        backButton.layer.cornerRadius = 5.0
        backButton.layer.borderWidth = 1
        backButton.layer.borderColor = UIColor.white.cgColor
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated:true)
        
    }

    // MARK: - TableView delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SaleDetailListCell") as! SaleDetailListCell
        
        if indexPath.row == 1 {
            cell.lblRefunded.isHidden = false
        } else {
            cell.lblRefunded.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }

}
