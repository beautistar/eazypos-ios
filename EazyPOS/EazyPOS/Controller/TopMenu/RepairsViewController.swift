//
//  RepairsViewController.swift
//  EazyPOS
//
//  Created by Beautistar on 29/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Presentr

class RepairsViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tblReparList: UITableView!
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.transitionType = TransitionType.coverHorizontalFromRight
        presenter.dismissOnSwipe = true
        return presenter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblReparList.tableFooterView = UIView()
        tblReparList.layer.borderWidth = 2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        initView()
    }
    
    func initView() {
        
        backButton.layer.cornerRadius = 5.0
        backButton.layer.borderWidth = 1
        backButton.layer.borderColor = UIColor.white.cgColor
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func newRepairAction(_ sender: Any) {
        
        let newRepairVC = self.storyboard?.instantiateViewController(withIdentifier: "NewRepairViewController") as! NewRepairViewController
        
        // Create the dialog
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage:0.6), height: ModalSize.fluid(percentage:0.9), center: ModalCenterPosition.center)
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        presenter.cornerRadius = 10
        presenter.roundCorners = true
        customPresentViewController(presenter, viewController: newRepairVC, animated: true, completion: nil)
    }
    

    // MARK: - SearchBar delegate
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        /*
         filteredData = searchText.isEmpty ? data : data.filter { (item: String) -> Bool in
         // If dataItem matches the searchText, return true to include it
         return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
         }
         
         tableView.reloadData()
         */
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    // MARK: - TableView delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepairListCell") as! RepairListCell
        cell.btnView.tag = indexPath.row
        cell.btnCheckOut.tag = indexPath.row
        
        cell.btnView.addTarget(self, action: #selector(viewTapped(_:)), for: .touchUpInside)
        
        cell.btnCheckOut.addTarget(self, action: #selector(checkoutTapped(_:)), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    @objc func viewTapped(_ sender:UIButton) {
        
        print(sender.tag)
        
        let repairDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "RepairDetailViewController") as! RepairDetailViewController
        repairDetailVC._selectedRepair = sender.tag
        
        // Create the dialog
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage:0.6), height: ModalSize.fluid(percentage:0.9), center: ModalCenterPosition.center)
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        presenter.cornerRadius = 10
        presenter.roundCorners = true
        customPresentViewController(presenter, viewController: repairDetailVC, animated: true, completion: nil)
        
    }
    
    @objc func checkoutTapped(_ sender:UIButton) {
        
        print(sender.tag)
        
    }
}
