//
//  AddPurchaseViewController.swift
//  EazyPOS
//
//  Created by Beautistar on 07/07/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class AddPurchaseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var imvPassportChk: UIImageView!
    @IBOutlet weak var imvOtherSpecifyChk: UIImageView!
    @IBOutlet weak var imvUtilityBillChk: UIImageView!
    @IBOutlet weak var imvDrivingLicenseChk: UIImageView!
    
    
    @IBOutlet weak var tfPassport: UITextField!
    @IBOutlet weak var tfDriverLicense: UITextField!
    @IBOutlet weak var tfUtilityBill: UITextField!
    @IBOutlet weak var tfOtherSpecify: UITextField!
    
    var passportChkd:Bool = false
    var drivingLisChkd:Bool = false
    var utilityBillChkd:Bool = false
    var OtherSpecifyChkd:Bool = false
    
    
    @IBOutlet weak var backButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initView()
    }
    
    func initView() {
        
        imvPassportChk.image = UIImage(named:"")
        imvDrivingLicenseChk.image = UIImage(named:"")
        imvUtilityBillChk.image = UIImage(named:"")
        imvOtherSpecifyChk.image = UIImage(named:"")
        
        tfPassport.isEnabled = false
        tfDriverLicense.isEnabled = false
        tfUtilityBill.isEnabled = false
        tfOtherSpecify.isEnabled = false
        
       
    }
    
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PurchaseItemCell") as! PurchaseItemCell
        return cell
    }

    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func checkAction(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            
            if passportChkd {
                imvPassportChk.image = UIImage(named:"")
                tfPassport.isEnabled = false
                tfPassport.text = ""
            } else {
                imvPassportChk.image = #imageLiteral(resourceName: "ic_check")
                tfPassport.isEnabled = true
            }
            passportChkd = !passportChkd
            break
            
        case 2:
            if drivingLisChkd {
                imvDrivingLicenseChk.image = UIImage(named:"")
                tfDriverLicense.isEnabled = false
                tfDriverLicense.text = ""
            } else {
                imvDrivingLicenseChk.image = #imageLiteral(resourceName: "ic_check")
                tfDriverLicense.isEnabled = true
            }
            drivingLisChkd = !drivingLisChkd
            break
            
        case 3:
            if utilityBillChkd {
                imvUtilityBillChk.image = UIImage(named:"")
                tfUtilityBill.isEnabled = false
                tfUtilityBill.text = ""
            } else {
                imvUtilityBillChk.image = #imageLiteral(resourceName: "ic_check")
                tfUtilityBill.isEnabled = true
            }
            utilityBillChkd = !utilityBillChkd
            break
            
        default:
            if OtherSpecifyChkd {
                imvOtherSpecifyChk.image = UIImage(named:"")
                tfOtherSpecify.isEnabled = false
                tfOtherSpecify.text = ""
            } else{
                imvOtherSpecifyChk.image = #imageLiteral(resourceName: "ic_check")
                tfOtherSpecify.isEnabled = true
            }
            
            OtherSpecifyChkd = !OtherSpecifyChkd
            break
        }
        
        
    }
}
