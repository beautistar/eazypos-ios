//
//  RecentSalesViewController.swift
//  EazyPOS
//
//  Created by Beautistar on 29/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import DropDown

class RecentSalesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tblRecentSaleList: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    
    var filterDropDown:DropDown!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupFilterDropDown()
    }
    
    func initView() {
        
        backButton.layer.cornerRadius = 5.0
        backButton.layer.borderWidth = 1
        backButton.layer.borderColor = UIColor.white.cgColor
        
        filterButton.layer.cornerRadius = 5.0
        filterButton.layer.borderWidth = 1
        filterButton.layer.borderColor = UIColor.white.cgColor        
    }
    
    func setupFilterDropDown() {
        
        filterDropDown = DropDown()
        filterDropDown.anchorView = UIView()
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        filterDropDown.bottomOffset = CGPoint(x: 0+40, y: 0)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        
        var _today = Date()
        var _dateArray = [String]()
        for _ in 1...20 {
            let tomorrow = Calendar.current.date(byAdding: .day, value: -1, to: _today)
            let date = DateFormatter()
            date.dateFormat = "EEEE, dd MMM yyyy"
            let stringDate : String = date.string(from: _today)
            _today = tomorrow!
            _dateArray.append(stringDate)
        }
        print(_dateArray)
        
       /*
        filterDropDown.dataSource = [
            "Tuesday, 30 May 2017",
            "Monday, 29 May 2017",
            "Sunday, 28 May 2017",
            "Saturday, 27 May 2017",
            "Friday, 26 May 2017",
            "Thursday, 25 May 2017",
            "Wednesday, 24 May 2017",
            "Tuesday, 23 May 2017",
            "Monday, 22 May 2017",
            "Sunday, 21 May 2017"
            
        ]
    */
        filterDropDown.dataSource = _dateArray
        filterDropDown.dismissMode = .onTap
        filterDropDown.direction = .bottom
        
        filterDropDown.cellNib = UINib(nibName:"FilterDropItem", bundle: nil)
        
        filterDropDown.customCellConfiguration = { (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? FilterDropItem else { return }
            
            // Setup your custom UI components
            cell.suffixLabel.text = "\(item)"
            cell.optionLabel.text = "\(1)"
//            cell.optionLabel.text = "Option \(index)"
//            cell.lblCout.text = item
        }
        
        // Action triggered on selection
        filterDropDown.selectionAction = { [unowned self] (index, item) in
            self.filterButton.setTitle(item, for: .normal)
            print(item)
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated:true)
    }
    
    @IBAction func filterAction(_ sender: Any) {
        
        filterDropDown.show()
    }
        // MARK: - SearchBar delegate
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        /*
         filteredData = searchText.isEmpty ? data : data.filter { (item: String) -> Bool in
         // If dataItem matches the searchText, return true to include it
         return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
         }
         
         tableView.reloadData()
         */
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    // MARK: - TableView delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepairListCell") as! RepairListCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
