//
//  AddCustomerViewController.swift
//  EazyPOS
//
//  Created by Beautistar on 28/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class AddCustomerViewController: BaseViewController {

    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var tfAddress1: UITextField!
    @IBOutlet weak var tfAddress2: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfPostalCode: UITextField!
    @IBOutlet weak var tfBusinessType: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var tfDob: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btnDob: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func initView() {
        
        tfName.layer.borderWidth = 2.0
        tfAddress1.layer.borderWidth = 2.0
        tfAddress2.layer.borderWidth = 2.0
        tfCity.layer.borderWidth = 2.0
        tfCountry.layer.borderWidth = 2.0
        tfPostalCode.layer.borderWidth = 2.0
        tfBusinessType.layer.borderWidth = 2.0
        tfPhoneNumber.layer.borderWidth = 2.0
        tfEmail.layer.borderWidth = 2.0
        
        tfName.layer.borderColor = UIColor.white.cgColor
        tfAddress1.layer.borderColor = UIColor.white.cgColor
        tfAddress2.layer.borderColor = UIColor.white.cgColor
        tfCity.layer.borderColor = UIColor.white.cgColor
        tfCountry.layer.borderColor = UIColor.white.cgColor
        tfPostalCode.layer.borderColor = UIColor.white.cgColor
        tfBusinessType.layer.borderColor = UIColor.white.cgColor
        tfPhoneNumber.layer.borderColor = UIColor.white.cgColor
        tfEmail.layer.borderColor = UIColor.white.cgColor
    }
    
    @IBAction func dobAction(_ sender: Any) {
        
        self.view.endEditing(true)
        
        let datePicker = ActionSheetDatePicker(title: "Date:", datePickerMode: UIDatePickerMode.date, selectedDate: NSDate() as Date!, doneBlock: {
            picker, value, index in
            
            /*
            print("value = \(value)")
            print("index = \(index)")
            print("picker = \(picker)")
            */
            //            let dateString = value as! String
            let dateFormatter = DateFormatter()
            //            dateFormatter.dateFormat = "EEE, dd MMM yyyy hh:mm:ss +zzzz"
            
            dateFormatter.dateFormat = "dd-MM-yyyy"
            dateFormatter.locale = Locale.init(identifier: "en_GB")
            //            let dateObj = dateFormatter.date(from: dateString)
            
            //            dateFormatter.dateFormat = "MM-dd-yyyy"
            print("Dateobj: \(dateFormatter.string(from: value! as! Date))")
            
            let dateStr = dateFormatter.string(from:value! as! Date)
            //            let words = dateStr.components(separatedBy: " ")
            
            self.tfDob.text = dateStr
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin:btnDob)
        let secondsInHundYear: TimeInterval = 100 * 365 * 24 * 60 * 60;
        datePicker?.minimumDate = NSDate(timeInterval: -secondsInHundYear, since: NSDate() as Date) as Date!
        datePicker?.maximumDate = NSDate(timeInterval: 0, since: NSDate() as Date) as Date!
        
        datePicker?.show()
    }

    @IBAction func countryAction(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: "Select Country", rows: ["UK", "US", "China"], initialSelection: 0, doneBlock: {
            picker, value, index in
            
            print("value = \(value)")
            print("index = \(String(describing: index))")
            print("picker = \(String(describing: picker))")
            
            self.tfCountry.text = index as? String
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
