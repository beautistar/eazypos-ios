//
//  ReceiptViewController.swift
//  EazyPOS
//
//  Created by Beautistar on 26/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class ReceiptViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        changeDueOpened = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)        
    }
}
