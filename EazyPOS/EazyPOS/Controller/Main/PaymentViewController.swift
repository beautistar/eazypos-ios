//
//  PaymentViewController.swift
//  EazyPOS
//
//  Created by Beautistar on 25/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {

    @IBOutlet weak var tfAmount: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if changeDueOpened {
            
            changeDueOpened = false
            self.navigationController?.popViewController(animated: false)            
        }
    }
    
    @IBAction func keyTapped(_ sender: Any) {
        
        let key = sender as! UIButton
        
        print(key.title)
        
        let string = tfAmount.text!.sanitized() + (key.titleLabel?.text)!
        tfAmount.text = string.currency()
        
    }
    
    @IBAction func cashAction(_ sender: Any) {
        
        let receiptVC = self.storyboard?.instantiateViewController(withIdentifier: "ReceiptViewController") as! ReceiptViewController
        self.present(receiptVC, animated:true)
        
    }

    @IBAction func deleteKeyTapped(_ sender: Any) {
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    
    }

}
