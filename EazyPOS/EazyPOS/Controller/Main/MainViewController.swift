//
//  MainViewController.swift
//  EazyPOS
//
//  Created by Beautistar on 6/5/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import Presentr
import MGSwipeTableCell

class MainViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, MGSwipeTableCellDelegate, UISearchBarDelegate  {
    
    @IBOutlet weak var tblItem: UITableView!
    @IBOutlet weak var cvProduct: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var w:CGFloat = 0.0
    var h:CGFloat = 0.0
    
    var isSearchShown:Bool=false
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.transitionType = TransitionType.coverHorizontalFromRight
        presenter.dismissOnSwipe = true
        return presenter
    }()
    
    fileprivate let configuration: PasscodeLockConfigurationType
    
    init(configuration: PasscodeLockConfigurationType) {
        
        self.configuration = configuration
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        let repository = UserDefaultsPasscodeRepository()
        repository.savePasscode(["1", "2", "3", "4"])
        configuration = PasscodeLockConfiguration(repository: repository)
        
        super.init(coder: aDecoder)
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        tblItem.tableFooterView = UIView()
        
        w = (self.view.frame.size.width - 60 - 2.5) / 6.0
        h = (self.view.frame.size.height - 100) / 8.0 - 30
        
        openDrawer()
    }
    
    func openDrawer() {
        
        self.perform(#selector(showDrawer(_:)), with: nil, afterDelay: 0.1)
    }
    
    func showDrawer(_ animated: Bool = true) {
        
        let drawerVC = self.storyboard?.instantiateViewController(withIdentifier: "DrawerViewController") as! DrawerViewController
        
        // Create the dialog
        
        presenter.presentationType = .custom(width: ModalSize.custom(size: 600.0)   , height: ModalSize.custom(size: 600), center: ModalCenterPosition.center)
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: drawerVC, animated: true, completion: nil)
    }
   
    @IBAction func closeAction(_ sender: Any) {
        
        let repository = UserDefaultsPasscodeRepository()
        repository.savePasscode(["1", "2", "3", "4"])
        let _configuration = PasscodeLockConfiguration(repository: repository)
        
        let passcodeVC: PasscodeLockViewController
        
        passcodeVC = PasscodeLockViewController(state: .enterPasscode, configuration: _configuration)
        
        //passcodeVC = PasscodeLockViewController(state: .removePasscode, configuration: configuration)
        
        passcodeVC.successCallback = { lock in
            
            print("success code : ")
            print(lock.repository.passcode ?? "---")
            
            lock.repository.deletePasscode()
            
        }
        
        present(passcodeVC, animated: true, completion: nil)
    }
    
    @IBAction func showSearchACtion(_ sender:Any) {
        
        if (isSearchShown) {
            
            searchBar.isHidden = true
        } else {
            searchBar.isHidden = false
        }
        
        isSearchShown = !isSearchShown
        
        self.productDetail()
        
    }
    
    func productDetail() {
        
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductDetailPopViewController") as! ProductDetailPopViewController
        
        // Create the dialog
        
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage: 0.8), height: ModalSize.custom(size: 350), center: ModalCenterPosition.center)
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: detailVC, animated: true, completion: nil)
        
    }
    
    @IBAction func recentSaleAction(_ sender: Any) {
    
        let recentSaleVC = self.storyboard?.instantiateViewController(withIdentifier:"RecentSalesViewController") as! RecentSalesViewController
        
        self.navigationController?.pushViewController(recentSaleVC, animated: true)
    }
    
    @IBAction func settingAction(_ sender: Any) {
        
        let settingVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingMainViewController") as! SettingMainViewController
        self.present(settingVC, animated: false, completion:nil)
    }
    
    @IBAction func addCustomerAction(_ sender: Any) {
        
        let addCustomerVC = self.storyboard?.instantiateViewController(withIdentifier: "AddCustomerViewController") as! AddCustomerViewController
        
        // Create the dialog        
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage:0.6), height: ModalSize.fluid(percentage:0.95), center: ModalCenterPosition.center)
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        presenter.cornerRadius = 10
        presenter.roundCorners = true
        customPresentViewController(presenter, viewController: addCustomerVC, animated: true, completion: nil)
    }
    
    @IBAction func parkAction(_ sender: Any) {
        
        let saveParkNameVC = self.storyboard?.instantiateViewController(withIdentifier: "SaveParkNameViewController") as! SaveParkNameViewController
        
        // Create the dialog
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage:0.6), height: ModalSize.fluid(percentage:0.2), center: ModalCenterPosition.center)
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        presenter.cornerRadius = 10
        presenter.roundCorners = true
        customPresentViewController(presenter, viewController: saveParkNameVC, animated: true, completion: nil)
        
    }
    
    @IBAction func voidAction(_ sender: Any) {
        
        showAlertDialog(title: nil, message: R.string.VOID_SALE, positive: R.string.YES, negative: R.string.NO)
        
    }
    
    // MARK: - SearchBar delegate
    @IBAction func gotoParkedSalesVC(_ sender: Any) {
        
        let parkedSalesVC = self.storyboard?.instantiateViewController(withIdentifier: "ParkedSaleViewController") as! ParkedSaleViewController
        self.navigationController?.pushViewController(parkedSalesVC, animated: true)
    }
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
        /*
         filteredData = searchText.isEmpty ? data : data.filter { (item: String) -> Bool in
         // If dataItem matches the searchText, return true to include it
         return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
         }
         
         tableView.reloadData()
         */
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
        searchBar.text = ""
        searchBar.resignFirstResponder()
    }
    

    // MARK: collection view datasource
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return _newsfeeds.count
        return 48
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    // MARK: TableView datasource & Delegate
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return 7
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50.0
        
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let itemCell = tableView.dequeueReusableCell(withIdentifier: "ItemListCell") as! MGSwipeTableCell
        itemCell.selectionStyle = UITableViewCellSelectionStyle.none
        itemCell.delegate = self
        //configure left buttons
        itemCell.rightButtons = [MGSwipeButton(title: "", icon: UIImage(named:"ic_close"), backgroundColor: .clear)  {
                                    (sender: MGSwipeTableCell!) -> Bool in
                                        print("Convenience callback for swipe buttons!")
                                        self.itemClose(indexPath.row)
                                        return true
                                    },
                                 MGSwipeButton(title: "", icon: UIImage(named:"ic_edit"), backgroundColor: .clear )  {
                                    (sender: MGSwipeTableCell!) -> Bool in
                                        print("Convenience callback for swipe buttons!")
                                        self.itemEdit(indexPath.row)
                                        return true
            }]
        itemCell.rightSwipeSettings.transition = .rotate3D
        itemCell.rightSwipeSettings.enableSwipeBounces = false
        
        return itemCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func itemClose(_ index:Int) {
        
        print("item closed at " + String(index))
    }
    
    func itemEdit(_ index:Int) {
        print("item Edit at " + String(index))
        
        let editVC = self.storyboard?.instantiateViewController(withIdentifier: "ProductEditViewController") as! ProductEditViewController
        
        // Create the dialog
        
        presenter.presentationType = .custom(width: ModalSize.custom(size: 500.0)   , height: ModalSize.custom(size: 600), center: ModalCenterPosition.center)
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: editVC, animated: true, completion: nil)
        
    }

}

extension MainViewController : UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: CGFloat(w), height: CGFloat(h))
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}
