//
//  RegisterViewController.swift
//  EazyPOS
//
//  Created by Beautistar on 22/06/2017.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class RegisterViewController: BaseViewController {

    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfCompany: UITextField!
    @IBOutlet weak var tfPhoneNumber: UITextField!
    @IBOutlet weak var backButton: PasscodeSignButton!
    @IBOutlet weak var tfBusinessType: UITextField!
    @IBOutlet weak var btnBusinessType: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initView() {
        
        backButton.layer.cornerRadius = 5.0
        backButton.layer.borderWidth = 1.0
        backButton.layer.borderColor = UIColor.white.cgColor
        
        tfUserName.layer.borderWidth = 2.0
        tfEmail.layer.borderWidth = 2.0
        tfPassword.layer.borderWidth = 2.0
        tfCompany.layer.borderWidth = 2.0
        tfPhoneNumber.layer.borderWidth = 2.0
        tfBusinessType.layer.borderWidth = 2.0
        
        tfUserName.layer.borderColor = UIColor.white.cgColor
        tfEmail.layer.borderColor = UIColor.white.cgColor
        tfPassword.layer.borderColor = UIColor.white.cgColor
        tfCompany.layer.borderColor = UIColor.white.cgColor
        tfPhoneNumber.layer.borderColor = UIColor.white.cgColor
        tfBusinessType.layer.borderColor = UIColor.white.cgColor

    }
    
    @IBAction func businessTypeAction(_ sender: Any) {
        
        ActionSheetStringPicker.show(withTitle: "Business Type", rows: ["AAA", "BBB", "CCC"], initialSelection: 0, doneBlock: {
            picker, value, index in
            
            print("value = \(value)")
            print("index = \(String(describing: index))")
            print("picker = \(String(describing: picker))")
            
            self.tfBusinessType.text = index as? String
            
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated:true)
    }
}
