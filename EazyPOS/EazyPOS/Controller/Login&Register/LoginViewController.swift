//
//  LoginViewController.swift
//  EazyPOS
//
//  Created by Beautistar on 5/31/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        tfUserName.layer.borderWidth = 2.0
        tfPassword.layer.borderWidth = 2.0
        tfUserName.layer.borderColor = UIColor.white.cgColor
        tfPassword.layer.borderColor = UIColor.white.cgColor
        
    }
    
    /*
    func updatePasscodeView() {
        
        let hasPasscode = configuration.repository.hasPasscode
        
        print("passcode 1: ")
        print(configuration.repository.passcode ?? "---")
        
        if (hasPasscode) {
        } else {
            
        }
    }
    */
    
    @IBAction func loginAction(_ sender: Any) {
        
//        let passcodeVC: PasscodeLockViewController
//        
//        
//        passcodeVC = PasscodeLockViewController(state: .setPasscode, configuration: configuration)
//            
//        //passcodeVC = PasscodeLockViewController(state: .removePasscode, configuration: configuration)
//        
//        passcodeVC.successCallback = { lock in
//            
//            print("success code : ")
//            print(lock.repository.passcode ?? "---")
//            
//            let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
//            self.navigationController?.pushViewController(mainVC, animated: false)
//            
//            lock.repository.deletePasscode()
//            
//        }
//        
//        present(passcodeVC, animated: true, completion: nil)

        
    }
    
    @IBAction func powerAction(_ sender: Any) {
        
        exit(0)
    }
}
